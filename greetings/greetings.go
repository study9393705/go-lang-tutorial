package greetings

import (
	"errors"
	"fmt"
	"math/rand"
)

// Hello returns a greeting for the named person.
func Hello(name string) (string, error) {
	// If no name was given, return an error with a message.
	if name == "" {
		return name, errors.New("empty name")
	}
	// Create a message using a random format.
	// message := fmt.Sprintf(randomFormat(), name)
	message := fmt.Sprint(randomFormat())
	return message, nil
}

// Hellos returns a map that associates each of the named people
// with a greeting message.
func Hellos(names []string) (map[string]string, error) {
	// A map to associate names with messages.
	messages := make(map[string]string)
	// Loop through the received slice of names, calling
	// the Hello function to get a message for each name.
	for _, name := range names {
		message, err := Hello(name)
		if err != nil {
			return nil, err
		}
		// In the map, associate the retrieved message with
		// the name.
		messages[name] = message
	}
	return messages, nil
}

// randomFormat returns one of a set of greeting messages. The returned
// message is selected at random.
func randomFormat() string {
	// A slice of message formats.
	formats := []string{
		"Hi, %v. Welcome!",
		"Great to see you, %v!",
		"Hail, %v! Well met!",
	}

	// Return one of the message formats selected at random.
	return formats[rand.Intn(len(formats))]
}

// Discover the Go install path, where the go command will install the current package.

// You can discover the install path by running the go list command, as in the following example:

// $ go list -f '{{.Target}}'

// For example, the command's output might say /home/gopher/bin/hello,
// meaning that binaries are installed to /home/gopher/bin.
// You'll need this install directory in the next step

// Add the Go install directory to your system's shell path.

// That way, you'll be able to run your program's executable without specifying where the executable is.

//     On Linux or Mac, run the following command:

//     $ export PATH=$PATH:/path/to/your/install/directory

// 	As an alternative, if you already have a directory like $HOME/bin in your shell path
// 	and you'd like to install your Go programs there,
// 	you can change the install target by setting the GOBIN variable using the go env command:

// 	$ go env -w GOBIN=/path/to/your/bin
